# -*- coding: utf-8 -*-

from django.contrib import admin

from . import models


@admin.register(models.Profile)
class ProfileAdmin(admin.ModelAdmin):

    list_display = (
        'id',
        'user',
        'get_user_first_name',
        'get_user_last_name',
        'get_user_email',
        'monthly_income',
        'financial_dependents',
        'expected_monthly_savings',
        'birth_date',
        'postal_code'
    )
    list_filter = (
        'id',
        'user',
        'monthly_income',
        'financial_dependents',
        'expected_monthly_savings',
        'birth_date',
        'postal_code'
    )
    list_display_links = (
        'id',
        'user'
    )

    def get_user_first_name(self, obj):
        return obj.user.first_name
    get_user_first_name.admin_order_field  = 'user__first_name'
    get_user_first_name.short_description = 'First Name'

    def get_user_last_name(self, obj):
        return obj.user.last_name
    get_user_last_name.admin_order_field  = 'user__last_name'
    get_user_last_name.short_description = 'Last Name'

    def get_user_email(self, obj):
        return obj.user.email
    get_user_email.admin_order_field  = 'user__email'
    get_user_email.short_description = 'Email'
