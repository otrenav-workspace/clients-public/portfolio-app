# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class Profile(models.Model):
    #
    # In Django user model:
    # - First name
    # - Last name
    # - Email
    #
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE
    )
    birth_date = models.DateField(
        blank=True,
        null=True
    )
    #
    # Optional
    #
    postal_code = models.CharField(
        max_length=5,
        blank=True,
        null=True
    )
    monthly_income = models.IntegerField(
        blank=True,
        null=True
    )
    financial_dependents = models.IntegerField(
        blank=True,
        null=True
    )
    expected_monthly_savings = models.IntegerField(
        blank=True,
        null=True
    )

#
# NOTE: This hooks will trigger everytime a `User` instance
# is saved There's should not be a need to trigger saves in
# `Profile` instances directly
#
@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
