# -*- coding: utf-8 -*-

USER_FIELDS = [
    'first_name',
    'last_name',
    'username',
    'email'
]

PROFILE_FIELDS = [
    'birth_date',
    'postal_code',
    'monthly_income',
    'financial_dependents',
    'expected_monthly_savings'
]

NULLABLE_FIELDS = [
    'DateField',
    'IntegerField'
]
