# -*- coding: utf-8 -*-

from django.contrib.auth import get_user_model

from rest_framework import serializers

USER_MODEL = get_user_model()


class UserSerializer(serializers.ModelSerializer):

    password = serializers.CharField(write_only=True)

    class Meta:
        model = USER_MODEL
        fields = ('id', 'email', 'username', 'password')
