# -*- coding: utf-8 -*-

from django.conf.urls import include, url
from django.contrib import admin


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^internals/users/',
        include('users.urls', namespace='users')),
    url(r'^internals/investments/',
        include('investments.urls', namespace='investments'))
]

