
| [Website](http://links.otrenav.com/website) | [Twitter](http://links.otrenav.com/twitter) | [LinkedIn](http://links.otrenav.com/linkedin)  | [GitHub](http://links.otrenav.com/github) | [GitLab](http://links.otrenav.com/gitlab) | [CodeMentor](http://links.otrenav.com/codementor) |

---

# Portfolio app proof-of-concept (backend)

- Omar Trejo
- January, 2017
- Build with Django

---

> "The best ideas are common property."
>
> —Seneca
