# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.db.models.signals import pre_delete, post_delete, post_save
from django.dispatch import receiver


class Movement(models.Model):

    # TODO: To whom? Application date?

    # Inbound or outbound
    inbound = models.BooleanField(
        blank=False,
        null=False
    )
    # TODO: Should this be named money/value?
    amount = models.FloatField(
        blank=False,
        null=False
    )
    successful = models.BooleanField(
        blank=False,
        null=False
    )
    date_time = models.DateTimeField(
        blank=False,
        null=False,
        auto_now_add=True
    )

    def quantity(self):
        """Return the quantity bought as a function of the date
        and money spent on a given transaction. Equivalent to
        number of shares bought."""
        composition_ids = Composition.movements.through.objects.filter(
            movement_id=self.pk
        ).values_list('composition_id', flat=True)

        if len(composition_ids) > 1:
            raise ValueError("Multiple `Composition` instances " +
                             "related to single `Movement` instance")

        composition = Composition.objects.get(id=composition_ids[0])

        # NOTE: This object's dates should be only for valid
        #       close price dates. Transactions movements should
        #       be executed only during available dates for trading,
        #       and hence have close prices

        # TODO (CRITICAL): Ask for data to simulate some portfolios
        #       and test that the results are accurate by comparison
        price = composition.fund.close_for_date(self.date_time)
        quantity = self.amount / price.close
        return(quantity)

    def __unicode__(self):
        name = str(self.id)
        if self.inbound:
            name += ": Inbound / "
        else:
            name += ": Outboud / "
        name += str(self.amount) + " / "
        if self.successful:
            name += ": Successful"
        else:
            name += ": Failed"
        return(unicode(name))


class Price(models.Model):

    get_latest_by = "date"

    date = models.DateField(
        blank=False,
        null=False
    )
    close = models.FloatField(
        blank=False,
        null=False
    )

    def __unicode__(self):
        return(unicode(str(self.close)))


class Fund(models.Model):

    name = models.CharField(
        max_length=100,
        blank=False,
        null=False
    )
    # TODO: Ticker needed?
    ticker = models.CharField(
        max_length=10,
        blank=False,
        null=False
    )
    organization = models.CharField(
        max_length=50,
        blank=False,
        null=False
    )
    prices = models.ManyToManyField(
        Price,
        blank=True
    )

    # TODO: Get all prices on create
    # TODO: Get all prices periodically and check diferences?

    def latest_close(self):
        return(self._latest_price().close)

    def close_for_date(self, date):
        if date.date() == timezone.now().date():
            # Avoid date not existing yet
            return(self._latest_price())
        return(self.prices.get(date=date))

    def _latest_price(self):
        price_ids = (
            Fund.prices.through.objects
            .filter(fund_id=self.pk)
            .values_list('price_id', flat=True)
        )
        latest_price = (
            Price.objects
            .filter(id__in=price_ids)
            .order_by('-date')
            [0]
        )
        return(latest_price)

    def __unicode__(self):
        return(unicode(
            str(self.id) + ": " +
            self.organization + " - " +
            self.name + " (" +
            self.ticker + ")"
        ))


class Composition(models.Model):

    # TODO: Many funds? Only two?
    # TODO: Dates of buy/sells?

    fund = models.ForeignKey(
        Fund,
        blank=False
    )
    movements = models.ManyToManyField(
        Movement,
        related_name='composition'
    )

    def invested(self):
        """Get the net invested money"""
        # TODO: Refactor with `self.performance()`
        movement_ids = Composition.movements.through.objects.filter(
            composition_id=self.pk
        )
        movements = Movement.objects.filter(id__in=movement_ids)
        invested = 0
        for movement in movements:
            if movement.successful:
                # TODO: Verify that only two possibilities can occur
                #       inbound and not inbound == outbound
                if movement.inbound:
                    invested += movement.amount
                if not movement.inbound:
                    invested -= movement.amount
        return(invested)

    def performance(self):
        """Get the net performance by getting
        net quantity x current price"""
        # TODO: Refactor with `self.invested()`
        movement_ids = Composition.movements.through.objects.filter(
            composition_id=self.pk
        )
        movements = Movement.objects.filter(id__in=movement_ids)
        quantity = 0
        for movement in movements:
            if movement.successful:
                # TODO: Verify that only two possibilities can occur
                #       inbound and not inbound == outbound
                if movement.inbound:
                    quantity += movement.quantity()
                if not movement.inbound:
                    quantity -= movement.quantity()
        performance = (
            quantity * self._latest_available_price() - self.invested()
        )
        return(performance)

    def total(self):
        return(self.invested() + self.performance())

    def goal_percentage(self):
        """If the investment has a goal, return it. Otherwise
        return only a `None` value that represents the lack
        of a goal"""
        investment = Investment.objects.get(composition=self)
        with_goal = investment.qualification.with_goal
        goal = investment.qualification.dynamic_field
        if with_goal:
            return(round(self.total() / goal, 2) * 100)
        return(None)

    def _latest_available_price(self):
        # TODO: Return latest recorded price  in case
        #       we don't have one for today? What happens
        #       if it's a weekend (no price available)?
        return(self.fund.latest_close())

    def __unicode__(self):
        return(unicode(str(self.id) + ": Composition"))


class Event(models.Model):

    successful = models.BooleanField(
        blank=False,
        null=False
    )
    message = models.CharField(
        max_length=500,
        blank=True,
        null=False
    )
    date_time = models.DateTimeField(
        blank=False,
        null=False,
        auto_now_add=True
    )

    def __unicode__(self):
        name = str(self.id) + ": " + self.message + " / "
        if self.successful:
            name += "Successful"
        else:
            name += "Failed"
        return(unicode(name))


class Qualification(models.Model):

    # Whether or not a goal is associated
    with_goal = models.BooleanField(
        blank=False
    )
    # Risk aversion (qualitative question)
    aversion = models.IntegerField(
        # Level 1: Most risk averse
        # Level 4: Least risk averse
        blank=False,
        null=False
    )
    # Initial investment in MXN $
    initial_investment = models.IntegerField(
        blank=False,
        null=False
    )
    # Montly investment in MXN $ or Goal in MXN $
    # May represent `goal` (true) or `monthly_investment`
    # (false) depending on the value of `with_goal`
    dynamic_field = models.IntegerField(
        blank=False,
        null=False
    )
    # Expected number of years
    horizon = models.IntegerField(
        blank=False,
        null=False
    )
    # Percentage of acceptable gain/loss
    volatility = models.IntegerField(
        blank=False,
        null=False
    )

    def __unicode__(self):
        name = str(self.id) + ": "
        if self.with_goal:
            name += "With Goal / "
        else:
            name += "Without Goal / "
        name += str(self.aversion) + " / "
        name += str(self.horizon) + " / "
        name += str(self.volatility)
        return(unicode(name))


class Investment(models.Model):

    user = models.ForeignKey(
        User
    )
    composition = models.OneToOneField(
        Composition
    )
    events = models.ManyToManyField(
        Event
    )
    qualification = models.OneToOneField(
        Qualification
    )
    #
    # TODO: Should we check for correctness?
    # TODO: Should we send alerts if this changes?
    #
    # status = models.ForeignKey(
    #     Status
    # )
    name = models.CharField(
        max_length = 50,
        blank=False,
        null=False
    )
    creation_date_time = models.DateTimeField(
        blank=False,
        null=False,
        auto_now_add=True
    )
    # TODO: Current?
    # TODO: Max?
    # TODO: Removed?

    def __unicode__(self):
        name = self.user.username + " / "
        name += str(self.composition) + " / "
        name += str(self.qualification) + " / "
        return(unicode(name))


@receiver(pre_delete, sender=Composition)
def pre_delete_composition(sender, instance, *args, **kwargs):
    """Delete `Movement`'s related to `Composition`
    instance that will be deleted"""
    if instance.movements:
        movement_ids = Composition.movements.through.objects.filter(
            composition_id=instance.pk
        ).values_list('movement_id', flat=True)
        Movement.objects.filter(id__in=movement_ids).delete()


@receiver(pre_delete, sender=Investment)
def pre_delete_investment(sender, instance, *args, **kwargs):
    """Delete `Event`'s related to `Investment`
    instance that will be deleted"""
    if instance.events:
        event_ids = Investment.events.through.objects.filter(
            investment_id=instance.pk
        ).values_list('event_id', flat=True)
        Event.objects.filter(id__in=event_ids).delete()


@receiver(post_delete, sender=Investment)
def post_delete_investment(sender, instance, *args, **kwargs):
    """Delete `Composition` and `Qualification` related to
    `Investment` instance that will be deleted"""
    if instance.composition:
        instance.composition.delete()
    if instance.qualification:
        instance.qualification.delete()


@receiver(pre_delete, sender=Fund)
def post_delete_investment(sender, instance, *args, **kwargs):
    """Delete `Price`'s related to `Fund`
    instance that will be deleted"""
    if instance.prices:
        price_ids = Fund.prices.through.objects.filter(
            fund_id=instance.pk
        ).values_list('price_id', flat=True)
        Price.objects.filter(id__in=price_ids).delete()
