# -*- coding: utf-8 -*-

from django.conf.urls import url

from . import viewsets


urlpatterns = [
    url(r'^new/$', viewsets.NewInvestmentViewSet.as_view()),
    url(r'^transfer/$', viewsets.TransferViewSet.as_view()),
    url(r'^data/(?P<username>\w+)/$', viewsets.InvestmentsViewSet.as_view()),
]
