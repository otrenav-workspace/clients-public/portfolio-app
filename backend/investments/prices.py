# -*- coding: utf-8 -*-

import math
import numpy
import datetime

from yahoo_finance import Share

import models


class PricesCreator(object):

    """Retrieve and save prices for the last year since the creation
    of the fund. When this class gets called, it should retrieve
    prices for YTD.

    For now the mechanism is working using a ticker to retrieve the
    prices. However this will change as we'll use funds, not shares.
    """

    def __init__(self, fund):
        self.prices = None
        self.fund = fund

    def save_prices_year_to_date(self):
        """Get prices for year to date. This method
        is callend when creating a new fund.
        """
        # TODO: One year back is enough? How long back?
        # TODO: How to know how back can we go? API?
        self._get_prices(self._year_to_date(), self._today())
        self._save_price_closes_to_fund()

    def save_prices_last_to_date(self):
        """Get prices from last saved price to date. This
        method is called to update prices of existing funds.
        """
        self._get_prices(self._latest_date_in_fund(), self._today())
        self._save_price_closes_to_fund()

    def _get_prices(self, from_date, to_date):
        self._get_share_prices_from_yahoo_finance(from_date, to_date)

    def _get_share_prices_from_yahoo_finance(self, from_date, to_date):
        prices = Share(self.fund.ticker)
        self.prices = prices.get_historical(from_date, to_date)

    def _save_price_closes_to_fund(self):
        """Save price closes to fund. Assumes a structure in
        price data as a list of dictionaries with `close` and `date`:
            [ { u'Close': u'35.83', u'Date': u'2014-04-29', ... },
              ...
              { u'Close': u'33.99', u'Date': u'2014-04-28', ... },
            ]
        """
        for price in self.prices:
            new_price = models.Price.objects.get_or_create(
                close=price['Close'],
                date=price['Date']
            )
            new_price = new_price[0]
            self.fund.prices.add(new_price)

    def _latest_date_in_fund(self):
        price_ids = models.Fund.prices.through.object.filter(
            fund_id=self.fund.id
        ).values_list('price_id', flat=True)
        latest_price = models.Price.objects.filter(
            id__in=price_ids
        ).order_by['-date'][0]
        return(latest_price.date.strftime('%Y-%m-%d'))

    def _today(self):
        return(datetime.datetime.now().strftime('%Y-%m-%d'))

    def _year_to_date(self):
        today = datetime.datetime.now()
        return(str(today.year - 1) + '-' + today.strftime('%m-%d'))


class PricesPredictor(object):

    def __init__(self, dates, closes, horizon_percentage=0.2):
        self.dates = dates
        self.closes = closes
        self.extra_periods = int(math.ceil(len(dates) * horizon_percentage))

    def expand_dates(self):
        """EXpand dates by adding a new date for each period (day)"""
        new_dates = self.dates[:]
        latest_date = self.dates[-1]
        for i in range(self.extra_periods):
            new_dates.append(latest_date + datetime.timedelta(days=(i + 1)))
        return(new_dates)

    def expand_closes(self):
        """Expand closes by adding empty (`None`) values"""
        extra_closes = [None] * self.extra_periods
        return(self.closes + extra_closes)

    def optimistic_closes(self):
        # TODO: Ask for algorithm
        latest_close = self.closes[-1]
        standard_deviation = numpy.std(self.closes)
        optimistic_closes = [None] * int(len(self.closes) - 1)
        optimistic_closes.append(latest_close)

        for i in range(self.extra_periods):
            coefficient = standard_deviation / 2 * math.log(i + 1)
            optimistic_closes.append(latest_close + coefficient)
        return(optimistic_closes)

    def pesimistic_closes(self):
        # TODO: Ask for algorithm
        latest_close = self.closes[-1]
        standard_deviation = numpy.std(self.closes)
        pesimistic_closes = [None] * int(len(self.closes) - 1)
        pesimistic_closes.append(latest_close)

        for i in range(self.extra_periods):
            coefficient = standard_deviation / 2 * math.log(i + 1)
            pesimistic_closes.append(latest_close - coefficient)
        return(pesimistic_closes)
