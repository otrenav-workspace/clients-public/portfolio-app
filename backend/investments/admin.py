# -*- coding: utf-8 -*-

from django.contrib import admin

from . import models
from . import prices


@admin.register(models.Movement)
class MovementAdmin(admin.ModelAdmin):

    list_display = (
        'id',
        'inbound',
        'amount',
        'date_time',
        'successful'
    )
    list_filter = (
        'id',
        'inbound',
        'amount',
        'date_time',
        'successful'
    )
    list_display_links = (
        'id',
        'inbound',
        'amount',
        'date_time',
        'successful'
    )


@admin.register(models.Price)
class PriceAdmin(admin.ModelAdmin):

    list_display = (
        'date',
        'close'
    )
    list_filter = (
        'date',
        'close'
    )
    list_display_links = (
        'date',
        'close'
    )


@admin.register(models.Fund)
class FundAdmin(admin.ModelAdmin):

    list_display = (
        'id',
        'name',
        'ticker',
        'organization'
    )
    list_filter = (
        'id',
        'name',
        'ticker',
        'organization'
    )
    list_display_links = (
        'id',
        'name',
        'ticker',
        'organization'
    )

    def save_related(self, request, form, formsets, change):
        """Override `save_related` method to add `Price` instances
        to `Fund` dynamically. It can't be done by overriding the
        `save()` method in the model, as M2M relations can't be
        saved that way.

        http://timonweb.com/posts/many-to-many-field-save
        -method-and-the-django-admin/
        """
        super(FundAdmin, self).save_related(request, form, formsets, change)
        number_of_previous_prices = models.Fund.prices.through.objects.filter(
            fund_id=form.instance.id
        ).count()
        if number_of_previous_prices == 0:
            prices.PricesCreator(form.instance).save_prices_year_to_date()


@admin.register(models.Composition)
class CompositionAdmin(admin.ModelAdmin):

    list_display = (
        'id',
        'fund'
    )
    list_filter = (
        'id',
        'fund'
    )
    list_display_links = (
        'id',
        'fund'
    )


@admin.register(models.Event)
class EventAdmin(admin.ModelAdmin):

    list_display = (
        'id',
        'successful',
        'message',
        'date_time'
    )
    list_filter = (
        'id',
        'successful',
        'message',
        'date_time'
    )
    list_display_links = (
        'id',
        'successful',
        'message',
        'date_time'
    )


@admin.register(models.Qualification)
class QualificationAdmin(admin.ModelAdmin):

    list_display = (
        'id',
        'with_goal',
        'aversion',
        'initial_investment',
        'dynamic_field',
        'horizon',
        'volatility'
    )
    list_filter = (
        'id',
        'with_goal',
        'aversion',
        'initial_investment',
        'dynamic_field',
        'horizon',
        'volatility'
    )
    list_display_links = (
        'id',
        'with_goal',
        'aversion',
        'initial_investment',
        'dynamic_field',
        'horizon',
        'volatility'
    )


@admin.register(models.Investment)
class InvestmentAdmin(admin.ModelAdmin):

    list_display = (
        'id',
        'name',
        'user',
        'composition',
        'qualification'
    )
    list_filter = (
        'id',
        'name',
        'user',
        'composition',
        'qualification'
    )
    list_display_links = (
        'id',
        'name',
        'user',
        'composition',
        'qualification'
    )
