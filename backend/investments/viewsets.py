# -*- coding: utf-8 -*-


from django.contrib.auth import get_user_model
from django.db import transaction

from rest_framework import permissions, viewsets
from rest_framework.response import Response
from rest_framework.views import APIView

from users import models as user_models
from . import models


USER_MODEL = get_user_model()


class NewInvestmentViewSet(APIView):

    @transaction.atomic
    def post(self, request):
        self.request_data = request.data
        error = self.check_data()
        if error:
            return(Response({ 'status': 'error', 'message': error }))
        self.setup_initial_objects()
        self.save_birthdate_if_necessary()
        self.setup_movements()
        self.setup_events()
        self.setup_fund()
        self.setup_composition()
        self.setup_qualification()
        self.create_new_investment()
        return(Response({
            'status': 'OK',
            'message': 'Inversión creada. ¡Felicidades!'
        }))

    def check_data(self):
        # TODO: Check fund existence
        # TODO: Add all needed checks
        error = self.check_goal_larger_than_initial_investment()
        if error:
            return(error)
        return(None)

    def check_goal_larger_than_initial_investment(self):
        initial_investment = self.request_data['new_investment']['initial_investment']
        dynamic_field = self.request_data['new_investment']['dynamic_field']
        with_goal = self.request_data['new_investment']['with_goal']
        if with_goal and float(dynamic_field) <= float(initial_investment):
            return('Tu meta debe ser mayor a tu inversión inicial.')
        return(None)

    def setup_initial_objects(self):
        self.inv = self.request_data['new_investment']
        self.user = USER_MODEL.objects.get(id=self.request_data['user_ID'])
        self.profile = user_models.Profile.objects.get(user=self.user)

    def save_birthdate_if_necessary(self):
        if not self.profile.birth_date:
            self.profile.birth_date = self.inv['birth_date']
            self.profile.save()

    def setup_movements(self):
        # TODO (CRITICAL): Verificatio mechanism for succesful
        self.initial_deposit_movement = models.Movement.objects.create(
            amount=self.inv['initial_investment'],
            successful=True,
            inbound=True
        )
        self.initial_deposit_movement.save()

    def setup_events(self):
        self.creation_event = models.Event.objects.create(
            message='Nueva inversión creada',
            successful=True
        )
        self.creation_event.save()

        # TODO (CRITICAL): Verificatio mechanism for succesful
        self.initial_deposit_event = models.Event.objects.create(
            successful=True,
            message=(
                'Inversión inicial de MXN $' +
                '{:,}'.format(self.inv['initial_investment'])
            )
        )
        self.initial_deposit_event.save()

    def setup_fund(self):
        # TODO (CRITICAL): mechanism to determine fund
        self.fund = models.Fund.objects.get(name='Google')

    def setup_composition(self):
        self.composition = models.Composition.objects.create(fund=self.fund)
        self.composition.movements.add(self.initial_deposit_movement)

    def setup_qualification(self):
        self.qualification = models.Qualification.objects.create(
            with_goal=self.inv['with_goal'],
            aversion=self.inv['aversion'],
            initial_investment=self.inv['initial_investment'],
            dynamic_field=self.inv['dynamic_field'],
            horizon=self.inv['horizon'],
            volatility=self.inv['volatility']
        )

    def create_new_investment(self):
        self.investment = models.Investment.objects.create(
            user=self.user,
            composition=self.composition,
            qualification=self.qualification,
            name=self.inv['investment_name']
        )
        self.investment.events.add(
            self.initial_deposit_event,
            self.creation_event
        )


class InvestmentsViewSet(APIView):

    def get(self, request, username=None):
        self._setup_initial_objects(username)
        investments = self._build_investments()
        return(Response({ 'investments': investments }))

    def _setup_initial_objects(self, username):
        user = USER_MODEL.objects.get(username=username)
        self.investments = models.Investment.objects.filter(user=user).values()

    def _build_investments(self):
        investments = []
        for investment in self.investments:
            self.investment = investment
            investment = {
                'id': self.investment['id'],
                'name': self.investment['name'],
                'creation_date_time': self.investment['creation_date_time'],
                'qualification': self._build_qualification(),
                'composition': self._build_composition(),
                'events': self._build_events()
            }
            investments.append(investment)
        return(investments)

    def _build_qualification(self):
        qual = models.Qualification.objects.get(
            id=self.investment['qualification_id']
        )
        qualification = {
            'initial_investment': qual.initial_investment,
            'dynamic_field': qual.dynamic_field,
            'volatility': qual.volatility,
            'with_goal': qual.with_goal,
            'aversion': qual.aversion,
            'horizon': qual.horizon
        }
        return(qualification)

    def _build_composition(self):
        self.composition = models.Composition.objects.get(
            id=self.investment['composition_id']
        )
        composition = {
            'goal_percentage': self.composition.goal_percentage(),
            'performance': self.composition.performance(),
            'invested': self.composition.invested(),
            'total': self.composition.total(),
            'fund': self._build_fund()
        }
        return(composition)

    def _build_fund(self):
        price_ids = models.Fund.prices.through.objects.filter(
            fund_id=self.composition.fund.id
        ).values_list('price_id', flat=True)
        prices = models.Price.objects.filter(id__in=price_ids).values()

        dates = []
        closes = []
        for price in prices:
            closes.append(price['close'])
            dates.append(price['date'])

        dates = list(reversed(dates))
        closes = list(reversed(closes))

        # NOTE: local import to avoid import loops
        from investments.prices import PricesPredictor
        prices_predictor = PricesPredictor(dates, closes)

        fund = {
            'organization': self.composition.fund.organization,
            'ticker': self.composition.fund.ticker,
            'name': self.composition.fund.name,
            'optimistic_closes': prices_predictor.optimistic_closes(),
            'pesimistic_closes': prices_predictor.pesimistic_closes(),
            'closes': prices_predictor.expand_closes(),
            'dates': prices_predictor.expand_dates()
        }
        return(fund)


    def _build_events(self):
        event_ids = models.Investment.events.through.objects.filter(
            investment_id=self.investment['id']
        ).values_list('event_id', flat=True)
        evts = models.Event.objects.filter(
            id__in=event_ids
        ).order_by('-date_time').values()
        events = []
        for evt in evts:
            event = {
                'successful': evt['successful'],
                'message': evt['message'],
                'date_time': evt['date_time']
            }
            events.append(event)
        return(events)


class TransferViewSet(APIView):

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        self.data = request.data['transfer']
        error = self._check_data()
        if error:
            self._event({ 'successful': False, 'message': error })
            return(Response({ 'status': 'error', 'message': error }))

        # TODO: Verify that there are only two options here
        if self.data['inbound']:
            message = self._process_inbound()
        if not self.data['inbound']:
            message = self._process_outbound()

        self._event({ 'successful': True, 'message': message })
        return(Response({ 'status': 'OK', 'message': message }))

    def _check_data(self):
        # TODO: Verify that there are only two options here
        # TODO: Get all cases that need to be verified
        if not self.data['inbound']:
            inv = models.Investment.objects.get(id=self.data['investment_id'])
            if inv.composition.total() < self.data['amount']:
                message = (
                    'Intentaste retirar MXN $' +
                    '{:,}'.format(self.data['amount']) +
                    ' pero tu inversión no tiene suficientes fondos'
                )
                return(message)

        return(None)

    def _process_inbound(self):
        # TODO (CRITICAL): Confirmation mechanism
        # TODO: Refactor with `self._process_outbound()`
        investment = models.Investment.objects.get(id=self.data['investment_id'])
        movement = models.Movement.objects.create(
            inbound=self.data['inbound'],
            amount=self.data['amount'],
            successful=True
        )
        investment.composition.movements.add(movement)
        return('Inversión de MXN $' + str(self.data['amount']) + ' realizada')

    def _process_outbound(self):
        # TODO (CRITICAL): Confirmation mechanism
        # TODO: Refactor with `self._process_inbound()`
        investment = models.Investment.objects.get(id=self.data['investment_id'])
        movement = models.Movement.objects.create(
            inbound=self.data['inbound'],
            amount=self.data['amount'],
            successful=True
        )
        investment.composition.movements.add(movement)
        # TODO: How many days for transactions?
        return('Retiro de MXN $' +
               str(self.data['amount']) +
               ' registrado. Se ejecutará durante los próximos 5 días.')

    def _event(self, event_dict):
        investment = models.Investment.objects.get(
            id=self.data['investment_id']
        )
        event = models.Event.objects.create(
            successful=event_dict['successful'],
            message=event_dict['message']
        )
        investment.events.add(event)
