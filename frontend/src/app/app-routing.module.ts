
import { NgModule }               from '@angular/core';
import { RouterModule, Routes }   from '@angular/router';

import { SiteComponent }          from './site/site.component';
import { HowItWorksComponent }    from './site/how-it-works/how-it-works.component';
import { InvestmentsComponent }   from './investments/investments.component';
import { NewInvestmentComponent } from './investments/new-investment/new-investment.component';
import { UserComponent }          from './user/user.component';
import { NewUserComponent }       from './user/new-user/new-user.component';
import { ProfileComponent }       from './user/profile/profile.component';
import { AdminComponent }         from './admin/admin.component';
import { QuestionsComponent }     from './questions/questions.component';


const routes: Routes = [
    { path: '', redirectTo: 'sitio', pathMatch: 'full'},
    { path: 'sitio',           component: SiteComponent },
    { path: 'inversiones',     component: InvestmentsComponent },
    { path: 'nueva-inversion', component: NewInvestmentComponent },
    { path: 'perfil',          component: ProfileComponent },
    { path: 'admin',           component: AdminComponent },
    { path: 'iniciar-sesion',  component: UserComponent },
    { path: 'abrir-cuenta',    component: NewUserComponent },
    { path: 'como-funciona',   component: HowItWorksComponent },
    { path: 'ayuda',           component: QuestionsComponent }
];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {}

