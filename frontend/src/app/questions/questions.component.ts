
import { Component, OnInit } from '@angular/core';

declare var $: any;


@Component({
    selector: 'app-questions',
    templateUrl: './questions.component.html',
    styleUrls: ['./questions.component.scss']
})
export class QuestionsComponent implements OnInit {

    private activeTab: string;

    constructor() {
        this.activeTab = 'faq';
    }

    ngOnInit(): void {
        $('ul.tabs').tabs();
    }

    private activateTab(tab: string): void {
        this.activeTab = tab;
    }

    private isTabActive(tab: string): boolean {
        return tab === this.activeTab;
    }
}
