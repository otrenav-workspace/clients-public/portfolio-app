
export class NewInvestment {

    constructor(
        public investment_name?: string,
        public with_goal?: boolean,
        public birth_date?: Date,
        public aversion?: number,
        public initial_investment?: number,
        public monthly_investment?: number,
        public goal?: number,
        public horizon?: number,
        public volatility?: number
    ) {}
}
