
import 'rxjs/add/operator/toPromise';

import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';

import { NewInvestment } from './new-investment';
import { prefixURL }     from '../../app.setup';
import { User }          from '../../user/user';


const NEW_INVESTMENT_FIELDS = [
    'investment_name',
    'with_goal',
    'birth_date',
    'aversion',
    'initial_investment',
    'dynamic_field',
    'horizon',
    'volatility'
];


@Injectable()
export class NewInvestmentService {

    private url = prefixURL + 'investments/new/';

    constructor(private http: Http) {}

    public newInvestment(user: User, form: any): Promise <string> {
        let newInvestment = this.prepareNewInvestment(user, form);
        return this.sendToBackend(user, newInvestment)
            .then(response => { return response; });
    }

    private prepareNewInvestment(user: User, form: any): NewInvestment {
        let newInvestment: NewInvestment = new NewInvestment();
        for (let i = 0; i < NEW_INVESTMENT_FIELDS.length; i++) {
            newInvestment[NEW_INVESTMENT_FIELDS[i]] = (
                form[NEW_INVESTMENT_FIELDS[i]]
            );
        }
        return newInvestment;
    }

    private sendToBackend(
        user: User,
        newInvestment: NewInvestment
    ): Promise <string> {
        let securedHeaders = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'JWT ' + user.token
        });
        console.log(newInvestment);
        return this.http
            .post(
                this.url,
                JSON.stringify({
                    user_ID: user.id,
                    new_investment: newInvestment
                }),
                { headers: securedHeaders }
            )
            .toPromise()
            .then(response => { return response.json() })
            .catch(error => {
                // TODO: Log error to backend
                console.log(error);
                return "Ocurrió un error. Estamos trabajando para arreglarlo.";
            });
    }
}
