
import { Component,
         OnInit,
         AfterViewInit,
         DoCheck }              from '@angular/core';
import { FormBuilder,
         FormGroup,
         Validators }           from '@angular/forms';

import { ProfileService }       from '../../user/profile/profile.service';
import { NewInvestmentService } from './new-investment.service';
import { UserService }          from '../../user/user.service';


declare var $: any;
declare var wNumb: any;
declare var noUiSlider: any;


@Component({
    selector: 'app-new-investment',
    templateUrl: './new-investment.component.html',
    styleUrls: ['./new-investment.component.scss']
})
export class NewInvestmentComponent implements OnInit, AfterViewInit {

    private slider: any;
    private form: FormGroup;
    private datePicker: any;
    private success: boolean;
    private message: string = '';
    private profileHasBirthDate: boolean = false;

    constructor(
        private fb: FormBuilder,
        private userService: UserService,
        private profileService: ProfileService,
        private newInvestmentService: NewInvestmentService
    ) {}

    ngOnInit() {
        this.buildForm();
        this.loadDateWidget();
        this.checkBirthDate();
    }

    ngDoCheck() {
        this.form.patchValue({ birth_date: this.datePicker.get() });
    }

    ngAfterViewInit(): void {
        this.setupSlider();
    }

    private buildForm(): void {
        this.form = this.fb.group({
            investment_name: ['', Validators.required],
            with_goal: true,
            birth_date: ['', Validators.required],
            aversion: ['', Validators.required],
            initial_investment: ['', Validators.required],
            dynamic_field: ['', Validators.required],
            horizon: ['', Validators.required],
            volatility: ['20', Validators.required]
        });
    }

    private newInvestment(form: any): void {
        this.newInvestmentService
            .newInvestment(
                this.userService.currentUser,
                this.form.value
            ).then(response => {
                this.message = response['message'];
                if (response['status'] == "OK") {
                    this.success = true;
                } else {
                    this.success = false;
                }
            });
    }

    private selectGoal(type: string): void {
        this.form.patchValue({ with_goal: type });
    }

    private selectAversion(type: number): void {
        this.form.patchValue({ aversion: type });
    }

    private setupSlider(): void {
        this.slider = document.getElementById('volatility-slider');
        noUiSlider.create(
            this.slider,
            {
                connect: true,
                step: 10,
                range: {
                    'min': -100,
                    'max':  100
                },
                start: [-20, 20],
                format: wNumb({
                    decimals: 0
                }),
                tooltips: false,
                pips: {
                    mode: 'positions',
                    values: [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
                    format: wNumb({
                        postfix: '%'
                    })
                }
            }
        );
        this.slider.noUiSlider.on('slide', (values: number, handle: number) => {
            this.sliderUpdate(values, handle);
        });
    }

    private sliderUpdate(values: number, handle: number): void {
        if (handle) {
            this.form.patchValue({ 'volatility': values[handle] });
            this.slider.noUiSlider.set([
                Math.min(-values[handle], 0),
                Math.max( values[handle], 0)
            ]);
        } else {
            this.slider.noUiSlider.set([
                Math.min( values[handle], 0),
                Math.max(-values[handle], 0)
            ]);
        }
    }

    private checkBirthDate(): void {
        // TODO: This logic should not be here
        let profile = this.profileService.getCurrentProfile();
        if (!(profile == null)) {
            this.profileHasBirthDate = profile.hasBirthDate();
            this.form.patchValue({ birth_date: profile.birth_date });
        } else {
            this.profileService.refreshProfile()
                .then(() => {
                    let profile = this.profileService.getCurrentProfile();
                    this.profileHasBirthDate = profile.hasBirthDate();
                    this.form.patchValue({ birth_date: profile.birth_date });
                });
        }
    }

    private loadDateWidget(): void {
        let datePicker = $('.datepicker').pickadate({
            monthsFull: [
                'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
                'Junio', 'Julio', 'Agosto', 'Septiembre',
                'Octubre', 'Noviembre', 'Diciembre'
            ],
            monthsShort: [
                'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
                'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'
            ],
            weekdaysFull: [
                'Domingo', 'Lunes', 'Martes', 'Miércoles',
                'Jueves', 'Viernes', 'Sábado'
            ],
            weekdaysShort: [
                'Dom', 'Lun', 'Mar', 'Mie',
                'Jue', 'Vie', 'Sab'
            ],
            today: 'Hoy',
            clear: 'Borrar',
            close: 'Cerrar',
            labelMonthNext: 'Siguiente',
            labelMonthPrev: 'Anterior',
            labelMonthSelect: 'Selecciona un mes',
            labelYearSelect: 'Selecciona un año',
            format: 'yyyy-mm-dd',
            min: undefined,
            // max: this.eighteenYearsAgo(),
            max: new Date(),
            selectMonths: true,
            selectYears: 100
        });
        this.datePicker = datePicker.pickadate('picker');
    }

    private eighteenYearsAgo(): Date {
        let eighteenYearsAgo = new Date();
        eighteenYearsAgo.setMonth(eighteenYearsAgo.getMonth() - 12 * 18);
        return eighteenYearsAgo;
    }

    private initialInvestment(option: string): number {
        if (this.form.value.initial_investment == '' ||
            this.form.value.initial_investment == null) {
            return 0;
        }
        if (option === 'with_gain') {
            return this.form.value.initial_investment * 2;
        }
        return this.form.value.initial_investment;
    }

    private investmentWithGoal(): boolean {
        return this.form.value.with_goal;
    }

    private dynamicFieldHeading(): string {
        if (this.investmentWithGoal()) {
            return "Meta (MXN $)"
        } else {
            return "Inversión mensual (MXN $)"
        }
    }
}
