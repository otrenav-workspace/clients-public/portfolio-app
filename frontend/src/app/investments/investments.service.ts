
import 'rxjs/add/operator/toPromise';

import { Injectable }     from '@angular/core';
import { Headers, Http }  from '@angular/http';
import { Router }         from '@angular/router';

import { CookieService }  from 'angular2-cookie/core';

import { Investment }     from './investment/investment';
import { Qualification }  from './investment/qualification/qualification';
import { Composition }    from './investment/composition/composition';
import { Event }          from './investment/events/event';
import { UserService }    from '../user/user.service';
import { prefixURL }      from '../app.setup';


@Injectable()
export class InvestmentsService {

    private currentInvestments: Investment[];
    private url = prefixURL + 'investments/data/';
    private headers = new Headers({ 'Content-Type': 'application/json' });

    constructor(
        private http: Http,
        private userService: UserService
    ) {}

    public getInvestmentsForCurrentUser(): Promise <Investment[]> {
        let user = this.userService.getCurrentUser()
        let URL = this.url + user.username + '/?format=json';
        let securedHeaders = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'JWT ' + user.token
        });
        return this.http
            .get(URL, { headers: securedHeaders })
            .toPromise()
            .then(response => {
                let investments = this.buildInvestments(response.json());
                this.currentInvestments = investments;
                return investments;
            })
            .catch(error => {
                // TODO: Log
                console.log(error);
                return "Ocurrió un error. Estamos trabajando para arreglarlo."
            });
    }

    public getInvestment(id: number): Investment {
        if (!(this.currentInvestments == null)) {
            for (let i = 0; i < this.currentInvestments.length; i++) {
                if (this.currentInvestments[i].id == id) {
                    return this.currentInvestments[i];
                }
            }
        }
        return new Investment();
    }

    private buildInvestments(object: any): Investment[] {
        let invs = object.investments;
        let investments: Investment[] = [];
        for (let i = 0; i < invs.length; i++) {
            investments.push(this.buildInvestment(invs[i]));
        }
        return investments;
    }

    private buildInvestment(object: any): Investment {
        let investment: Investment = Object.assign(
            new Investment(), object
        );
        investment.qualification = this.buildQualification(object.qualification);
        investment.composition = this.buildComposition(object.composition);
        investment.events = this.buildEvents(object.events);
        return investment;
    }

    private buildQualification(object: any): Qualification {
        let qualification: Qualification = Object.assign(
            new Qualification(), object
        );
        return qualification;
    }

    private buildComposition(object: any): Composition {
        let composition: Composition = Object.assign(
            new Composition(), object
        );
        return composition;
    }

    private buildEvents(objects: any): Event[] {
        let events: Event[] = [];
        for (let i = 0; i < objects.length; i++) {
            events.push(Object.assign(new Event(), objects[i]));
        }
        return events;
    }
}
