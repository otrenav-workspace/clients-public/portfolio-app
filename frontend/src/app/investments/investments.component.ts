
import { Component, OnInit, AfterViewInit } from '@angular/core';

import { InvestmentsService } from './investments.service';
import { Investment } from './investment/investment';

declare var $: any;


@Component({
    selector: 'app-investments',
    templateUrl: './investments.component.html',
    styleUrls: ['./investments.component.scss']
})
export class InvestmentsComponent implements OnInit {

    private investments: Investment[];

    constructor(private investmentsService: InvestmentsService) {}

    ngOnInit() {
        this.updateInvestments();
    }

    ngAfterViewInit(): void {
        this.activateGUIBehavior();
    }

    public updateInvestments(): void {
        this.investmentsService
            .getInvestmentsForCurrentUser()
            .then(investments => {
                this.investments = investments
                console.log(this.investments);
            });
    }

    private activateGUIBehavior(): void {
        $('.collapsible').collapsible();
    }

    private reloadTabs(): void {
        // NOTE: We need to add a timeout for the tabs
        //       to reload because the DOM does not yet
        //       have the elements it needs to reload
        //       when this function is called, but it has
        //       just a few miliseconds afterwards.
        setTimeout(function() { $('ul.tabs').tabs(); }, 10);
    }

    private total(metric: string): number {
        let total = 0;
        let number_of_investments_with_goal = 0;
        if (!(this.investments == null)) {
            for (let i = 0; i < this.investments.length; i++) {
                total += this.investments[i][metric]();
                if (this.investments[i].withGoal()) {
                    number_of_investments_with_goal += 1;
                }
            }
            if (metric === 'goalPercentage') {
                // total /= this.investments.length;
                if (number_of_investments_with_goal > 0) {
                    total /= number_of_investments_with_goal;
                } else {
                    total = 0;
                }
                total = Math.round(total);
            }
        }
        return total;
    }

    private atLeastOneGoal(): boolean {
        if (!(this.investments == null)) {
            for (let i = 0; i < this.investments.length; i++) {
                if (this.investments[i].withGoal()) {
                    return true;
                }
            }
        }
        return false;
    }

    private investmentsExist(): boolean {
        if (!(this.investments == null)) {
            return false;
        }
        return true;
    }
}
