
import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder,
         FormGroup,
         Validators }               from '@angular/forms';

import { TransferService }          from './transfer.service';
import { Investment }               from '../investment';


@Component({
    selector: 'app-transfer',
    templateUrl: './transfer.component.html',
    styleUrls: ['./transfer.component.scss']
})
export class TransferComponent implements OnInit {

    @Input() investment: Investment;

    private form: FormGroup;
    private success: boolean;
    private message: string = '';

    constructor(
        private fb: FormBuilder,
        private transferService: TransferService
    ) {}

    ngOnInit() {
        this.buildForm();
    }

    private buildForm(): void {
        this.form = this.fb.group({
            amount: ['', Validators.required]
        });
    }

    private transfer(form: any, investment: Investment, inbound: boolean): void {
        // TODO: Refactor logic with `this.receive()`
        this.transferService
            .transfer(form, investment, inbound)
            .then(response => {
                this.message = response['message'];
                if (response['status'] == "OK") {
                    this.success = true;
                } else {
                    this.success = false;
                }
            });
    }
}
