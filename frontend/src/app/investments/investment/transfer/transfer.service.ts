
import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';

// TODO: Remove complex path structures with index files
import { NewInvestment } from '../../new-investment/new-investment';
import { UserService }   from '../../../user/user.service';
import { prefixURL }     from '../../../app.setup';
import { Investment }    from '../investment';
import { Transfer }      from './transfer';


// const TRANSFER_FIELDS = ['amount', 'date'];
const TRANSFER_FIELDS = ['amount'];


@Injectable()
export class TransferService {

    private url = prefixURL + 'investments/transfer/';

    constructor(
        private http: Http,
        private userService: UserService
    ) {}

    public transfer(
        form: any,
        investment: Investment,
        inbound: boolean
    ): Promise <string> {
        let transfer = this.prepareTransfer(form, investment, inbound);
        return this.sendToBackend(transfer)
            .then(response => { return response; });
    }

    private prepareTransfer(
        form: any,
        investment: Investment,
        inbound: boolean
    ) {
        let transfer: Transfer = new Transfer();
        transfer.inbound = inbound;
        transfer.amount = form.value.amount;
        transfer.investment_id = investment.id;
        // for (let i = 0; i < TRANSFER_FIELDS.length; i++) {
        //     transfer[TRANSFER_FIELDS[i]] = form[TRANSFER_FIELDS[i]];
        // }
        console.log(transfer);
        return transfer;
    }

    private sendToBackend(transfer: Transfer): Promise <string> {
        // TODO: Refactor logic with other `sendToBackend` methods
        let user = this.userService.getCurrentUser();
        let securedHeaders = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'JWT ' + user.token
        });
        return this.http
            .post(
                this.url,
                JSON.stringify({ transfer: transfer }),
                { headers: securedHeaders }
            )
            .toPromise()
            .then(response => { return response.json() })
            .catch(error => {
                // TODO: Log error to backend
                console.log(error);
                return "Ocurrió un error. Estamos trabajando para arreglarlo.";
            });
    }
}
