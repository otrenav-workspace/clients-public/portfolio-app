
export class Transfer {

    constructor(
        public investment_id?: number,
        public inbound?: boolean,
        public amount?: string,
        public date?: Date
    ) {}
}
