
export class Fund {

    constructor(
        public name?: string,
        public ticker?: string,
        public organization?: string,
        public closes?: number[],
        public dates?: Date[],
        public optimistic_closes?: number[],
        public pesimistic_closes?: number[]
    ) {}
}
