
import { Component, OnInit, Input } from '@angular/core';

import { Investment } from '../investment';


@Component({
    selector: 'app-composition',
    templateUrl: './composition.component.html',
    styleUrls: ['./composition.component.scss']
})
export class CompositionComponent implements OnInit {

    @Input() investment: Investment;

    constructor() {}

    ngOnInit() {}
}
