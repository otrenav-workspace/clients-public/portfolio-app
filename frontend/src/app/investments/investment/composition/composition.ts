
import { Fund } from './fund';

export class Composition {

    constructor(
        public goal_percentage?: number,
        public performance?: number,
        public invested?: number,
        public total?: number,
        public fund?: Fund
    ) {}
}
