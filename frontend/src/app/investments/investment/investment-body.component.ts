
import { Component, OnInit, Input } from '@angular/core';

import { InvestmentsService } from '../investments.service';
import { Investment } from './investment';

declare var $: any;


@Component({
    selector: 'app-investment-body',
    templateUrl: './investment-body.component.html',
    styleUrls: ['./investment-body.component.scss']
})
export class InvestmentBodyComponent implements OnInit {

    @Input() investment: Investment;

    private activeTab: string;

    constructor(private investmentsService: InvestmentsService) {
        this.activeTab = 'performance';
    }

    ngOnInit(): void {}

    private activateTab(tab: string): void {
        this.activeTab = tab;
    }

    private isTabActive(tab: string): boolean {
        return tab === this.activeTab;
    }
}
