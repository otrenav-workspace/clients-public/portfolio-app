
import { Component, OnInit, Input } from '@angular/core';
import { Investment }               from '../investment';

// let template = require('./line-chart-demo.html');


@Component({
    selector: 'app-performance',
    templateUrl: './performance.component.html',
    styleUrls: ['./performance.component.scss']
})
export class PerformanceComponent implements OnInit {

    @Input() investment: Investment;

    private data: any[] = [
        {
            // data: [65, 59, 80, 81, null, null, null],
            data: [],
            label: 'Comportamiento histórico',
            pointRadius: 0,
            borderWidth: 3,
            fill: false
        },
        {
            // data: [null, null, null, 81, 100, 110, 115],
            data: [],
            label: 'Escenario optimista',
            pointRadius: 0,
            borderWidth: 3,
            borderDash: [10, 10],
            fill: false
        },
        {
            // data: [null, null, null, 81, 60, 50, 45],
            data: [],
            label: 'Escenario pesimista',
            pointRadius: 0,
            borderWidth: 3,
            borderDash: [10, 10],
            fill: false
        },
    ];
    private labels: any[] = [];
    private options: any = {
        animation: false,
        responsive: true
    };
    private colors: any[] = [
        { // Past performance (blue)
            borderColor:               '#82BDDE',
            backgroundColor:           '#82BDDE',
            pointBackgroundColor:      '#82BDDE',
            pointBorderColor:          '#FFFFFF',
            pointHoverBackgroundColor: '#B2FF59',
            pointHoverBorderColor:     '#B2FF59'
        },
        { // Positive scenario (green)
            borderColor:               '#8BC34A',
            backgroundColor:           '#8BC34A',
            pointBackgroundColor:      '#8BC34A',
            pointBorderColor:          '#FFFFFF',
            pointHoverBackgroundColor: '#B2FF59',
            pointHoverBorderColor:     '#B2FF59'
        },
        { // Negative scenario (red)
            borderColor:               '#F44336',
            backgroundColor:           '#F44336',
            pointBackgroundColor:      '#F44336',
            pointBorderColor:          '#FFFFFF',
            pointHoverBackgroundColor: '#B2FF59',
            pointHoverBorderColor:     '#B2FF59'
        }
    ];
    private legend: boolean = true;
    private type: string = 'line';

    constructor() {}

    ngOnInit() {
        this.setupPriceData();
    }

    private setupPriceData() {
        this.labels = this.investment.composition.fund.dates;
        this.data[0].data = this.investment.composition.fund.closes;
        this.data[1].data = this.investment.composition.fund.optimistic_closes;
        this.data[2].data = this.investment.composition.fund.pesimistic_closes;
    }
}
