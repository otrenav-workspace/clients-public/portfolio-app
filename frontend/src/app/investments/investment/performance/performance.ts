
export class Performance {

    constructor(
        public prices[]: number[],
        public upper_price_estimates[]: number[],
        public lower_price_estimates[]: number[],
        public dates[]: Date[]
    ) {}
}
