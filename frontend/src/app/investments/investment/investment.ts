
import { Qualification } from './qualification/qualification';
import { Performance }   from './performance/performance';
import { Composition }   from './composition/composition';
import { Transfer }      from './transfer/transfer';
import { Event }         from './events/event';


export class Investment {

    constructor(
        // public performance?: Performance,
        public id?: number,
        public name?: string,
        public creation_date_time?: Date,
        public qualification?: Qualification,
        public composition?: Composition,
        public events?: Event[]
    ) {}

    public invested(): number {
        return this.composition.invested;
    }

    public performance(): number {
        return this.composition.performance;
    }

    public total(): number {
        return this.composition.total;
    }

    public goalPercentage(): number {
        return this.composition.goal_percentage
    }

    public withGoal(): boolean {
        return this.qualification.with_goal;
    }
}
