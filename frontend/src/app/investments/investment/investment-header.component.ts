
import { Component, OnInit, Input } from '@angular/core';

import { InvestmentsService } from '../investments.service';
import { Investment } from './investment';


@Component({
    selector: 'app-investment-header',
    templateUrl: './investment-header.component.html',
    styleUrls: ['./investment-header.component.scss']
})
export class InvestmentHeaderComponent implements OnInit {

    @Input() investment: Investment;

    constructor(private investmentsService: InvestmentsService) {}

    ngOnInit() {}
}
