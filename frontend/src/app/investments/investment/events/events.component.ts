
import { Component, OnInit, Input } from '@angular/core';

import { Investment } from '../investment';


@Component({
    selector: 'app-events',
    templateUrl: './events.component.html',
    styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit {

    @Input() investment: Investment;

    constructor() {}

    ngOnInit() {}
}
