
export class Event {

    constructor(
        public successful?: boolean,
        public message?: string,
        public date_time?: Date
    ) {}

    public successString(): string {
        if (this.successful) {
            return "Éxito"
        }
        return "Fracaso"
    }
}
