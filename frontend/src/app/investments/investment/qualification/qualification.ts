
export class Qualification {

    constructor(
        public initial_investment?: number,
        public dynamic_field?: number,
        public with_goal?: boolean,
        public volatility?: number,
        public aversion?: number,
        public horizon?: number
    ) {}
}
