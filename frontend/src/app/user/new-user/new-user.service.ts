
import 'rxjs/add/operator/toPromise';

import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Router }        from '@angular/router';

import { prefixURL }     from '../../app.setup';


@Injectable()
export class NewUserService {

    private url = prefixURL + 'users/';
    private headers = new Headers({
        'Content-Type': 'application/json'
    });

    constructor(private http: Http) {}

    public newUser(
        email: string,
        username: string,
        password: string
    ): Promise <string> {
        return this.http
            .post(
                this.url + 'new-user/?format=json',
                JSON.stringify({
                    email: email,
                    username: username,
                    password: password
                }),
                { headers: this.headers }
            )
            .toPromise()
            .then(response => {
                return response.json();
            })
            .catch(error => {
                // TODO: Log error to backend
                console.log(error);
                return "Ocurrió un error. Estamos trabajando para arreglarlo.";
            });
    }
}

