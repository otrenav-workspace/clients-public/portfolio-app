
import { Router }                 from '@angular/router';
import { OnInit, Component }      from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { NewUserService }         from './new-user.service';
import { User }                   from '../user';

//
// TODO: Toda la lógica es la misma que la de sesión,
//       refactorizar hasta donde sea posible
//


@Component({
    selector: 'app-new-user',
    templateUrl: './new-user.component.html',
    styleUrls: ['./new-user.component.scss']
})
export class NewUserComponent {

    private form: FormGroup;
    private success: boolean;
    private message: string = '';

    constructor(
        private router: Router,
        private fb: FormBuilder,
        private newUserService: NewUserService
    ) {
        this.form = this.fb.group({
            email: '',
            username: '',
            password: ''
        });
    }

    newUser(): void {
        this.newUserService
            .newUser(
                this.form.value.email,
                this.form.value.username,
                this.form.value.password
            )
            .then(response => {
                this.message = response['message'];
                if (response['status'] == 'OK') {
                    this.success = true;
                } else {
                    this.success = false;
                }
            });
    }
}
