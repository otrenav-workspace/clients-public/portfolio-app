
import 'rxjs/add/operator/toPromise';

import { Injectable }     from '@angular/core';
import { Headers, Http }  from '@angular/http';
import { Router }         from '@angular/router';

import { CookieService }  from 'angular2-cookie/core';

// import { ProfileService } from './profile/profile.service';
import { prefixURL }      from '../app.setup';
import { User }           from './user';


@Injectable()
export class UserService {

    public currentCookie: any;
    public currentUser: User;

    // private url = 'users/';
    private url = prefixURL + 'users/';
    private headers = new Headers({ 'Content-Type': 'application/json' });

    constructor(
        private http: Http,
        private router: Router,
        private cookieService: CookieService
        // private profileService: ProfileService
    ) {}

    public getCurrentUser(): User {
        return this.currentUser;
    }

    public logout(): void {
        this.deleteCookie();
        this.setCurrentUser(null);
        // this.profileService.logout();
        this.router.navigate(['iniciar-sesion']);
    }

    public login(username: string, password: string): Promise <string> {
        return this.http
            .post(
                this.url + 'login/?format=json',
                {
                    headers: this.headers,
                    username: username,
                    password: password
                }
            )
            .toPromise()
            .then(response => {
                return this.buildUser(
                    username,
                    response.json().token
                ).then(user => {
                    this.currentUser = user;
                    this.cookieService.putObject(
                        'bursapp',
                            <Object> this.currentUser
                    );
                    return 'OK';
                })
            })
            .catch(error => {
                console.log(error);
                return 'Usuario o contraseña incorrectos';
            });
    }

    public validateUser(): boolean {
        if (this.currentUser == null && !this.getUserFromCookie()) {
            this.router.navigate(['iniciar-sesion']);
            return false;
        }
        return true;
    }

    public currentUserIsAdmin(): boolean {
        return this.userGroupChecker('Administrators');
    }

    private setCurrentUser(user: User): void {
        this.currentUser = user;
    }

    private deleteCookie(): void {
        this.cookieService.remove('bursapp');
    }

    private userLoggedIn(): boolean {
        return !(this.currentUser == null);
    }

    private buildUser(username: string, token: string): Promise <User> {
        let getUserIDURL = (
            this.url + 'get-user-info/' + username + '/?format=json'
        );
        return this.http
            .get(getUserIDURL)
            .toPromise()
            .then(response => {
                return new User(
                    response.json().id,
                    username,
                    token,
                    response.json().groups
                );
            });
    }

    private getUserFromCookie(): boolean {
        let c: any = this.cookieService.getObject('bursapp');
        if (!(c == null)) {
            this.currentUser = new User(c.id, c.username, c.token, c.groups);
        }
        if (!(this.currentUser == null)) {
            return true;
        }
        return false;
    }

    private userGroupChecker(group: string): boolean {
        if (this.userLoggedIn()) {
            for (let i = 0; i < this.currentUser.groups.length; i++) {
                if (this.currentUser.groups[i] == group) {
                    return true;
                }
            }
        }
        return false;
    }

    private handleError(error: any): Promise <any> {
        console.error('An error ocurred', error);
        return Promise.reject(error.message || error);
    }
}

