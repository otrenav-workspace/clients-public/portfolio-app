
import { Component, OnInit, DoCheck, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators }        from '@angular/forms';

import { Profile }                                   from './profile';
import { UserService }                               from '../user.service';
import { PROFILE_FIELDS }                            from './profile.service';
import { ProfileService }                            from './profile.service';

declare var $: any;
declare var Materialize: any;


@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, DoCheck, AfterViewInit {

    private form: FormGroup;
    private datePicker: any;
    private success: boolean;
    private message: string = '';

    constructor(
        private fb: FormBuilder,
        private userService: UserService,
        private profileService: ProfileService
    ) {}

    ngOnInit() {
        this.buildForm();
        this.loadDateWidget();
    }

    ngDoCheck() {
        this.form.value.birth_date = this.datePicker.get();
    }

    ngAfterViewInit() {
        this.populateForm();
    }

    private buildForm(): void {
        this.form = this.fb.group({
            first_name: '',
            last_name: '',
            username: ['', Validators.required],
            email: ['', Validators.required],
            birth_date: '',
            postal_code: '',
            monthly_income: '',
            financial_dependents: '',
            expected_monthly_savings: ''
        });
    }

    private populateForm(): void {
        this.profileService.refreshProfile().then(() => {
            this.form.setValue(this.profileService.getCurrentProfile());
            this.updateWidgets();
        });
    }

    private updateProfile(form: any): void {
        this.profileService
            .updateProfile(
                this.userService.currentUser,
                this.form.value
            ).then(response => {
                this.message = response['message'];
                if (response['status'] == "OK") {
                    this.success = true;
                } else {
                    this.success = false;
                }
            });
    }

    private loadDateWidget(): void {
        let datePicker = $('.datepicker').pickadate({
            monthsFull: [
                'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
                'Junio', 'Julio', 'Agosto', 'Septiembre',
                'Octubre', 'Noviembre', 'Diciembre'
            ],
            monthsShort: [
                'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
                'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'
            ],
            weekdaysFull: [
                'Domingo', 'Lunes', 'Martes', 'Miércoles',
                'Jueves', 'Viernes', 'Sábado'
            ],
            weekdaysShort: [
                'Dom', 'Lun', 'Mar', 'Mie',
                'Jue', 'Vie', 'Sab'
            ],
            today: 'Hoy',
            clear: 'Borrar',
            close: 'Cerrar',
            labelMonthNext: 'Siguiente',
            labelMonthPrev: 'Anterior',
            labelMonthSelect: 'Selecciona un mes',
            labelYearSelect: 'Selecciona un año',
            format: 'yyyy-mm-dd',
            min: undefined,
            // max: this.eighteenYearsAgo(),
            max: new Date(),
            selectMonths: true,
            selectYears: 100
        });
        this.datePicker = datePicker.pickadate('picker');
    }

    private eighteenYearsAgo(): Date {
        let eighteenYearsAgo = new Date();
        eighteenYearsAgo.setMonth(eighteenYearsAgo.getMonth() - 12 * 18);
        return eighteenYearsAgo;
    }

    private updateWidgets(): void {
        Materialize.updateTextFields();
    }
}
