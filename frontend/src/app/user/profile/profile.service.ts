
import 'rxjs/add/operator/toPromise';

import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';

import { UserService }   from '../user.service';
import { prefixURL }     from '../../app.setup';
import { Profile }       from './profile';
import { User }          from '../user';


export const PROFILE_FIELDS = [
    'first_name',
    'last_name',
    'username',
    'email',
    'birth_date',
    'postal_code',
    'monthly_income',
    'financial_dependents',
    'expected_monthly_savings'
];

@Injectable()
export class ProfileService {

    private currentProfile: Profile;
    private url = prefixURL + 'users/profile/';

    constructor(
        private http: Http,
        private userService: UserService
    ) {}

    public updateProfile(user: User, form: any): Promise <string> {
        let profile = this.prepareProfile(user, form);
        return this.sendToBackend(user, profile)
            .then(response => { return response; });
    }

    public logout(): void {
        this.setCurrentProfile(null);
    }

    public getCurrentProfile(): Profile {
        return this.currentProfile;
    }

    public refreshProfile(): Promise <string> {
        let user: User = this.userService.getCurrentUser();
        let getUserIDURL = (
            this.url + 'get-user-info/' + user.username + '/?format=json'
        );
        return this.http
            .get(getUserIDURL)
            .toPromise()
            .then(response => {
                this.buildProfile(response.json())
                return "OK";
            });
    }

    private buildProfile(response: Object): void {
        // TODO: Refactor with `prepareProfile` logic
        let profile: Profile = new Profile();
        for (let i = 0; i < PROFILE_FIELDS.length; i++) {
            profile[PROFILE_FIELDS[i]] = response[PROFILE_FIELDS[i]];
        }
        this.setCurrentProfile(profile);
    }

    private setCurrentProfile(profile: Profile): void {
        this.currentProfile = profile;
    }

    private prepareProfile(user: User, form: any): Profile {
        let profile: Profile = new Profile();
        for (let i = 0; i < PROFILE_FIELDS.length; i++) {
            profile[PROFILE_FIELDS[i]] = form[PROFILE_FIELDS[i]];
        }
        return profile;
    }

    private sendToBackend(user: User, profile: Profile): Promise <string> {
        let securedHeaders = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'JWT ' + user.token
        });
        return this.http
            .post(
                this.url,
                JSON.stringify({
                    profile: profile,
                    user_ID: user.id
                }),
                { headers: securedHeaders }
            )
            .toPromise()
            .then(response => { return response.json() })
            .catch(error => {
                // TODO: Log error to backend
                console.log(error);
                return "Ocurrió un error. Estamos trabajando para arreglarlo.";
            });
    }
}
