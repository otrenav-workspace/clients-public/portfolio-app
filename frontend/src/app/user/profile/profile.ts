
export class Profile {

    constructor(
        public first_name?: string,
        public last_name?: string,
        public username?: string,
        public email?: string,
        public birth_date?: Date,
        public postal_code?: string,
        public monthly_income?: number,
        public financial_dependents?: number,
        public expected_monthly_savings?: number
    ) {}

    public hasBirthDate(): boolean {
        return !(this.birth_date == null);
    }
}
