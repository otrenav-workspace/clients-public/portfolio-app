
import { Router }                 from '@angular/router';
import { OnInit, Component }      from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { UserService }         from './user.service';
import { User }                   from './user';


@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

    private form: FormGroup;
    private success: boolean;
    private message: string = '';

    constructor(
        private router: Router,
        private fb: FormBuilder,
        private userService: UserService
    ) {
        this.form = this.fb.group({username: '', password: ''});
    }

    ngOnInit(): void {
        this.logout();
    }

    logout(): void {
        this.userService.logout();
    }

    login(): void {
        this.userService
            .login(
                this.form.value.username,
                this.form.value.password
            )
            .then(message => {
                if (message === 'OK') {
                    this.message = '';
                    this.router.navigate(['/inversiones']);
                } else {
                    this.success = false;
                    this.message = message
                }
            });
    }
}
